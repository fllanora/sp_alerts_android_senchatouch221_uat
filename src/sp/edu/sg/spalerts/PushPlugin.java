package sp.edu.sg.spalerts;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.DropBoxManager;
import android.os.Environment;
import android.util.Log;

import org.apache.cordova.DroidGap;
import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.PluginResult;

import com.google.android.gcm.GCMRegistrar;

public class PushPlugin extends CordovaPlugin {

	private static final String TAG = null;
	private static String regToken = null;
	private static String messageID = "";
	private static String messageAlert = "";
	public static final String NOTIFICATION_DATA = "NOTIFICATION_DATA";
	private static boolean isPushRegistrationStarted = false;
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.phonegap.api.Plugin#execute(java.lang.String,
	 * org.json.JSONArray, java.lang.String)
	 */
	 @Override
	    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException 
	    {
	//@Override
	//public PluginResult execute(String action, JSONArray data, String callbackId) {
		 
		PluginResult result = null;
		// GCMIntentService gcmIntent;// = new GCMIntentService();

		/*
		 * Register the Device for Push Notification (C2DM)
		 */
		try {
			if (action.equals("registerDevice")) {
				if (!isPushRegistrationStarted) {
					Log.v(TAG, "LOG: register Device");
					isPushRegistrationStarted = true;
					GCMRegistrar.checkDevice(MainActivity.getAppContext());
					GCMRegistrar.checkManifest(MainActivity.getAppContext());
				
					final String regId = GCMRegistrar.getRegistrationId(MainActivity
							.getAppContext());
					if (regId.equals("")) {
						Log.v(TAG, "Not registered");
						GCMRegistrar.register(MainActivity.getAppContext(),
								"295234647339");
						do {
							try {
								Thread.sleep(1000);
							} catch (InterruptedException ex) {
								Thread.currentThread().interrupt();
							}

						} while (GCMIntentService.getRegistrationToken() == null);
						regToken = GCMIntentService.getRegistrationToken();
					} else {
						Log.v(TAG, "Already registered");
						regToken = regId;
					}
				
					this.echo(regToken, callbackContext);
					   return true;
				} else {
					regToken = GCMRegistrar.getRegistrationId(MainActivity
							.getAppContext());
			
					this.echo(regToken, callbackContext);
					   return true;
					
				}

			} else if (action.equals("getMessageAlert")) {
				// String messageData = // gcmInten
				// //getIntent().getExtras().getString(NOTIFICATION_DATA);
				messageAlert = MainActivity.messageData;
				Log.v(TAG, "LOG: getMessageAlert: " + messageAlert);
		
				this.echo(messageAlert, callbackContext);
				   return true;
			} else if (action.equals("getMessageId")) {
				messageID = MainActivity.messageID;
				Log.v(TAG, ">>>>>>>> LOG: getMessageId: " + messageID);

				this.echo(messageID, callbackContext);
				   return true;
			} else if (action.equals("setMessageId")) {
				// GCMIntentService.messageID = "0";
				// messageID = GCMIntentService.messageID;
				// //gcmIntent.messageID;
				MainActivity.messageData = "";
				MainActivity.messageID = "";
				messageID = MainActivity.messageID;
				Log.v(TAG, "LOG: resetMessageId: " + messageID);

				this.echo(messageID, callbackContext);
				   return true;
			} else {
				
				this.echo("", callbackContext);
			}

		} catch (Exception e) {
			this.echo("", callbackContext);
		}
		return false;
	}
	 
	 private void echo(String message, CallbackContext callbackContext) {
	      //  if (message != null && message.length() > 0) {
	            callbackContext.success(message);
	      //  } else {
	      //      callbackContext.error("Expected one non-empty string argument.");
	     //  }
	    }

}