package sp.edu.sg.spalerts;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;
import android.content.Context;
import android.os.Environment;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.apache.cordova.api.PluginResult;

public class OfflineCache extends CordovaPlugin {

	private static final String TAG = null;

	/******************************************************
	 * Offline Plugin
	 ******************************************************/
	//@Override
	//public PluginResult execute(String action, JSONArray args, String callbackId) {
	 @Override
	    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException 
	    {
		
	//	if (!action.equals("downloadFile"))
		//	return new PluginResult(PluginResult.Status.INVALID_ACTION);

		try {
			
			if(action.equals("getApplicationPackageName"))
			{
				/******************************************************
				 * Get Application Name
				 ******************************************************/
				String applicationName = MainActivity.getAppContext().getPackageName();
				applicationName = applicationName.replace(".","");
				applicationName = applicationName.replace("_","");
				Log.v(TAG, "LOG: application name folder: "+applicationName);
				//return new PluginResult(PluginResult.Status.OK, applicationName);
				  this.echo(applicationName, callbackContext);
			      return true;
				
			}
			
			if (action.equals("downloadFile"))
			{
				
			/******************************************************
			 * Download File
			 ******************************************************/
			Log.v(TAG, "LOG: downloadFile");
			String fileUrl = data.getString(0);
			JSONObject params = data.getJSONObject(1);
			
			String fileHeaderCookie = data.getString(2);
			
			Log.v(TAG, "LOG: fileHeaderCookie:"+fileHeaderCookie);
	
			/******************************************************
			 * Set Filename
			 ******************************************************/
			String fileName = params.has("fileName") ? params
					.getString("fileName") : fileUrl.substring(fileUrl
					.lastIndexOf("/") + 1);
					
					/******************************************************
					 * Get Application Name
					 ******************************************************/
					String applicationName = MainActivity.getAppContext().getPackageName();
					applicationName = applicationName.replace(".","");
					applicationName = applicationName.replace("_","");
					Log.v(TAG, "LOG: application name folder: "+applicationName);
					
					/******************************************************
					 * Set Directory
					 ******************************************************/
					String dirName = Environment.getExternalStorageDirectory().getPath();

		
			//String dirName = params.has("dirName") ? params
		//			.getString("dirName") : Environment
			//		.getExternalStorageDirectory().getPath(); // + "/download";

			Boolean overwrite = params.has("overwrite") ? params
					.getBoolean("overwrite") : false;

		//	return this.downloadUrl(fileUrl, dirName, fileName, overwrite,
		//			callbackId, fileHeaderCookie);
			
			
			try {

				Log.d("PhoneGapLog", "Downloading " + fileUrl + " into " + dirName
						+ "/" + fileName);

				File dir = new File(dirName);
				if (!dir.exists()) {
					Log.d("PhoneGapLog", "directory " + dirName + " created");
					dir.mkdirs();
				}

				/******************************************************
				 * Create the file
				 ******************************************************/
				File file = new File(dirName, fileName);

				if (!overwrite && file.exists()) {
					Log.d("DownloaderPlugin", "File already exist");

					JSONObject obj = new JSONObject();
					obj.put("status", 1);
					obj.put("total", 0);
					obj.put("file", fileName);
					obj.put("dir", dirName);
					obj.put("progress", 100);
					
					this.echo("success", callbackContext);
					   return true;

				//	return new PluginResult(PluginResult.Status.OK, obj);
				}
				/******************************************************
				 * Set the File URL
				 ******************************************************/
				URL url = new URL(fileUrl);
				HttpURLConnection ucon = (HttpURLConnection) url.openConnection();
				ucon.setRequestMethod("GET");
				ucon.addRequestProperty("Cookie", fileHeaderCookie);	
				ucon.connect();
		
				InputStream is = ucon.getInputStream();
				byte[] buffer = new byte[1024];
				int readed = 0, progress = 0, totalReaded = 0, fileSize = ucon
						.getContentLength();

				FileOutputStream fos = new FileOutputStream(file);
				/******************************************************
				 * Read the response stream
				 ******************************************************/
				while ((readed = is.read(buffer)) > 0) {

					fos.write(buffer, 0, readed);
					totalReaded += readed;

					int newProgress = (int) (totalReaded * 100 / fileSize);
					if (newProgress != progress)
						progress = informProgress(fileSize, newProgress, dirName,
								fileName, callbackContext);

				}

				fos.close();

				Log.d("PhoneGapLog", "Download finished");

				JSONObject obj = new JSONObject();
				obj.put("status", 1);
				obj.put("total", fileSize);
				obj.put("file", fileName);
				obj.put("dir", dirName);
				obj.put("progress", progress);

				//return new PluginResult(PluginResult.Status.OK, obj);
				this.echo("success", callbackContext);
				   return true;

			} catch (FileNotFoundException e) {
				Log.d("PhoneGapLog", "File Not Found: " + e);
			//	return new PluginResult(PluginResult.Status.ERROR, 404);
				this.echo("", callbackContext);
				   return true;
			} catch (IOException e) {
				Log.d("PhoneGapLog", "Error: " + e);
			//	return new PluginResult(PluginResult.Status.ERROR, e.getMessage());
				this.echo("", callbackContext);
				   return true;
			}

			
			}

		} catch (JSONException e) {

			e.printStackTrace();
			//return new PluginResult(PluginResult.Status.JSON_EXCEPTION,
				//	e.getMessage());
			this.echo("", callbackContext);
			   return true;

		} catch (InterruptedException e) {
			//e.printStackTrace();
			//return new PluginResult(PluginResult.Status.ERROR, e.getMessage());
			this.echo("", callbackContext);
			   return true;
		}
		//return null;
		return false;

	}
	 
	 private void echo(String message, CallbackContext callbackContext) {
	        if (message != null && message.length() > 0) {
	            callbackContext.success(message);
	        } else {
	            callbackContext.error("Expected one non-empty string argument.");
	        }
	    }

	/******************************************************
	 * Downlad the data based on the url
	 ******************************************************/
//	private PluginResult downloadUrl(String fileUrl, String dirName,
	//		String fileName, Boolean overwrite, String callbackId, String fileHeaderCookie)
	//		throws InterruptedException, JSONException {

	
	//}
	
	
    /******************************************************
    * Inform Progress
    ******************************************************/    
	private int informProgress(int fileSize, int progress, String dirName,
			String fileName, CallbackContext callbackContext) throws InterruptedException,
			JSONException {

		JSONObject obj = new JSONObject();
		obj.put("status", 0);
		obj.put("total", fileSize);
		obj.put("file", fileName);
		obj.put("dir", dirName);
		obj.put("progress", progress);

		//PluginResult res = new PluginResult(PluginResult.Status.OK, obj);
		//res.setKeepCallback(true);
		//success(res, callbackId);
		this.echo("success", callbackContext);
		 
		// Give a chance for the progress to be sent to javascript
		Thread.sleep(100);

		return progress;
	}
	
	 
}