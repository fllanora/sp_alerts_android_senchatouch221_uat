Ext.define('app.model.alertsListModel', {
    extend: 'Ext.data.Model',

    config: {
       fields : [
        {name : 'alert_Id', type:'string'},
        {name : 'alert_Source', type:'string'},
        {name : 'alert_Title', type:'string'},
        {name : 'alert_TitleShort', type:'string'},
        {name : 'alert_Description', type:'string'},
        {name : 'xml_Status', type:'string'},
        {name : 'xml_Status_Code', type:'string'},
        {name : 'last_Updated', type:'string'}

    ]
    }
});

